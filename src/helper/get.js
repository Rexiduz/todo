import axios from "axios";
import { url } from "../services";

async function HttpGetTodo(api) {
  const token = await localStorage.getItem("token");
  try {
    const auth = { headers: { Authorization: token } };
    return await axios.get(url + `/${api}`, auth);
  } catch (e) {
    return e;
  }
}

export default HttpGetTodo;
