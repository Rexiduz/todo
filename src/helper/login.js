import axios from "axios";
import { url } from "../services";

async function HttpPostLogin(api, obj) {
  let data = {};
  try {
    let res = await axios.post(url + `/${api}`, obj);
    data = { res, status: "success" };
  } catch (e) {
    data = { status: "error", massage: e };
  } finally {
    return data;
  }
}

export default HttpPostLogin;
