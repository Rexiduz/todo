import React, { useEffect, useState, useCallback } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { Navbar, Nav } from "react-bootstrap";

import { Page, TxtLink } from "./styles/MainLayout.js";

import Home from "./Pages/home";
import Login from "./Pages/login";
import Bonus from "./Pages/bonus";

import history from "./history";

export default function App() {
  const [expanded, setExpanded] = useState(false);

  const getToken = useCallback(async () => {
    var token = localStorage.getItem("token");
    if (token) {
      history.push("/");
      return;
    } else {
      history.push("/login");
      return;
    }
  }, []);

  useEffect(() => {
    getToken();
  }, [getToken]);

  const logout = () => {
    localStorage.removeItem("token");
    setExpanded(false);
  };

  return (
    <Router history={history}>
      <Page>
        <Navbar
          bg="info"
          collapseOnSelect
          expanded={expanded}
          variant="dark"
          expand="lg"
        >
          <Navbar.Brand href="#home">
            <b>MyTodo</b>
          </Navbar.Brand>

          <Navbar.Toggle
            onClick={() => setExpanded(expanded ? false : "expanded")}
            aria-controls="basic-navbar-nav"
          />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto" navbar>
              <Link
                to="/"
                onClick={() => setExpanded(expanded ? false : "expanded")}
              >
                <TxtLink>Home</TxtLink>
              </Link>

              <Link
                to="/bonus"
                onClick={() => setExpanded(expanded ? false : "expanded")}
              >
                <TxtLink>Bonus</TxtLink>
              </Link>
              <Link to="/login" onClick={logout}>
                <TxtLink>Logout</TxtLink>
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <Switch>
          <Route path="/Login">
            <Login />
          </Route>
          <Route path="/bonus">
            <Bonus />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Page>
    </Router>
  );
}
