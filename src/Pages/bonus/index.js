import React, { useState, useEffect, useCallback } from "react";
import { Form, Button } from "react-bootstrap";

import { Main, MyForm } from "../../styles/MainLayout.js";

export default () => {
  let [arr1, setArr1] = useState([1, 2, 3, 4, 5]);
  let [arr2, setArr2] = useState([1, 2]);

  let result = arr1.filter((e) => {
    let aaa = arr2.filter((a) => a === e);

    if (aaa.length > 0) {
      return aaa;
    }
  });

  console.log("RE: ", result);
  return (
    <Main>
      <h1>Bonus</h1>
      <div className="text-left pl-2">
        <p>Array1: {arr1}</p>
        <p>Array2: {arr2}</p>
      </div>

      {result &&
        result.map((res, index) => (
          <ul
            key={index}
            border="info"
            className="mb-2"
            style={{ maxWidth: "1200px" }}
          >
            <li>{res}</li>
          </ul>
        ))}
    </Main>
  );
};
