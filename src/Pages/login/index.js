import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Form, Button } from "react-bootstrap";
import { Main, MyForm } from "../../styles/MainLayout.js";

import HttpPostLogin from "../../helper/login.js";

export default () => {
  const router = useHistory();
  let [username, setUsername] = useState("");
  let [password, setPassword] = useState("");

  const handleChangeUsername = (event) => {
    setUsername(event.target.value);
  };

  const handleChangePass = (event) => {
    setPassword(event.target.value);
  };

  async function login() {
    if ((username, password)) {
      const obj = {
        username: username,
        password: password,
      };

      let response = await HttpPostLogin("/users/auth", obj);
      if (response.status === "success") {
        let token = response.res.data.token;
        localStorage.setItem("token", token);
        router.push("/");
      } else {
        router.push("/login");
      }
    } else {
      alert("Please input Email & Password");
    }
  }

  return (
    <Main>
      <MyForm>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={username}
            onChange={handleChangeUsername}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={handleChangePass}
          />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={login}>
          Submit
        </Button>
      </MyForm>
    </Main>
  );
};
